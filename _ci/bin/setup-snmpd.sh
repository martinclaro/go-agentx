#!/usr/bin/env bash

_afile=/etc/apt/sources.list
_bfile=/etc/snmp/snmp.conf
_cfile=/etc/snmp/snmpd.conf
_dfile=/etc/default/snmpd

cat "${_afile}" | grep '^deb ' | sed -e 's, main\([\t\s]*\)$, main contrib non-free,g' 1>"${_afile}.tmp"
mv -f "${_afile}.tmp" "${_afile}"

apt-get update -qq && \
    apt-get -y install \
        snmp \
        snmpd \
        libsnmp-base \
        snmp-mibs-downloader 2>/dev/null

sed -i -e "s,^export MIBS=\(.*\)$,export MIBS=\"UCD-SNMP-MIB\",g" "${_dfile}" 2>/dev/null
sed -i -e "s,^SNMPDOPTS=\(.*\)$,SNMPDOPTS='-Lf /tmp/snmpd.log -u root -g root -I -smux -p /var/run/snmpd.pid -Ducd-snmp/pass -Dagentx -x tcp:127.0.0.1:705 -a',g" "${_dfile}" 2>/dev/null
echo "" 1>>"${_dfile}" 2>/dev/null
echo "export MIBDIRS=/usr/share/mibs/netsnmp:/usr/share/mibs/iana:/usr/share/mibs/ietf:/usr/share/mibs" 1>>"${_dfile}" 2>/dev/null
echo "" 1>>"${_dfile}" 2>/dev/null

sed -i 's/^mibs/#mibs/g' "${_bfile}" 2>/dev/null

sed -i 's/^#rocommunity public  localhost/rocommunity public  localhost/g' "${_cfile}" 2>/dev/null

nohup /etc/init.d/snmpd restart 2>/dev/null
sleep 5

exit 0